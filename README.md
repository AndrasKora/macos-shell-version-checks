Shell script to list version information on OS components, mainly compilers and installed apps by Brew and Pip.

1 known issue:
- if Java is not installed on macOS Big Sur the check will state incorrect details for Java

Tested on macOS Monterey 12.3
